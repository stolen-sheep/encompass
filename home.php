<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package encompass
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<!-- <div id="accent_1" class="accent"></div>
			<div id="accent_2" class="accent"></div> -->
			<a href="https://usapayroll.myisolved.com/UserLogin.aspx" target="_blank" id="login_1"><button>iSolved Login</button></a>
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?><img id="logo" src="<?php echo get_template_directory_uri(); ?>/assets/encompass.svg"></a></h1>
			<ul id="contactList">
				<li id="contactPhone"><a href="tel:8772200072"><i class="fas fa-phone"></i> 877-220-0072</a></li>
				<li id="contactMail">
					<script type="text/javascript">
					user = "ihelp";
					domain = "usapayroll.com";
					document.write('<a href=\"mailto:' + user + '@' + domain + '\">');
					</script>
					<i class="fas fa-envelope"></i> Contact Us</a>
				</li>
			</ul>
			<ul id="hours">
				<li>Office Hours:</li>
				<li>Monday - Thursday 8:30am to 5:00pm</li>
				<li>&bull;</li>
				<li>Friday - 9am to 4pm</li><br>
				<li>Training and Sales Available Nights and Weekends</li>
			</ul>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
